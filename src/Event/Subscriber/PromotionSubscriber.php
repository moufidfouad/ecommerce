<?php

namespace App\Event\Subscriber;

use App\Entity\Promotion;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;

class PromotionSubscriber implements EventSubscriberInterface
{

    public function onPersist(BeforeEntityPersistedEvent $event)
    {
        $subject = $event->getEntityInstance();

        if(!$subject instanceof Promotion){
            return;
        }

        $this->changeItems($subject);
    }

    public function onUpdate(BeforeEntityUpdatedEvent $event)
    {
        $subject = $event->getEntityInstance();

        if(!$subject instanceof Promotion){
            return;
        }

        $this->changeItems($subject);
    }

    public function onDelete(BeforeEntityDeletedEvent $event)
    {
        $subject = $event->getEntityInstance();

        if(!$subject instanceof Promotion){
            return;
        }

        $this->changeItems($subject,true);
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['onPersist'],
            BeforeEntityUpdatedEvent::class => ['onUpdate'],
            BeforeEntityDeletedEvent::class => ['onDelete']
        ];
    }

    private function changeItems(Promotion $promotion,bool $onRemove = false)
    {
        foreach ($promotion->getItems() as $key => $value) {
            $produit = $value->getProduit();
            $produit->setDiscount($onRemove ? null : $value->getPrix());
            $produit->setEndDiscount($onRemove ? null : $promotion->getFin());
        }
    }
}