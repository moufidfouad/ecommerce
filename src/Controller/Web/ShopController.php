<?php
namespace App\Controller\Web;

use Twig\Environment;
use App\Entity\Search\VariationSearch;
use App\Repository\VariationRepository;
use App\Form\Search\VariationSearchType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;


/**
 * @Route("/shop")
 */
class ShopController
{
    /** @var FormFactoryInterface $form */
    private $form;
    /** @var Environment $engine */
    private $engine;
    /** @var PaginatorInterface $paginator */
    private $paginator;
    /** @var VariationRepository $variationRepository */
    private $variationRepository;
    public function __construct(
        FormFactoryInterface $form,
        Environment $engine,
        PaginatorInterface $paginator,
        VariationRepository $variationRepository
    ){
        $this->form = $form;
        $this->engine = $engine;
        $this->paginator = $paginator;
        $this->variationRepository = $variationRepository;
    }

    /**
     * @Route("", name="web_shop_index")
     */
    public function index(Request $request)
    {
        $search = new VariationSearch();
        $search->setPage($request->query->get('page',1));
        $search->setLimit($request->query->get('limit',3));
        $search->setOrder($request->query->get('order'));
        $searchForm = $this->form->create(VariationSearchType::class,$search);
        $searchForm->handleRequest($request);
        [$min , $max] = $this->variationRepository->findMinMax($search);
        $query = $this->variationRepository
            ->findBySearch($search)
            ->getQuery()
        ;
        $pager = $this->paginator->paginate($query,$search->getPage(),$search->getLimit());
        
        if($request->get('ajax')){
            return new JsonResponse([
                'content' => $this->engine->render('shop/includes/_list.html.twig',[
                    'items' => $pager
                ]),
                'pagination' => $this->engine->render('shop/includes/_pagination.html.twig',[
                    'items' => $pager
                ]),
                'pagesize' => $this->engine->render('shop/includes/_pagesizeselecter.html.twig'),
                'sorting' => $this->engine->render('shop/includes/_orderselecter.html.twig'),
                'min' => $min,
                'max' => ($min == $max) ? $min + 1 : $max
            ]);
        }
        return new Response($this->engine->render('shop/index/page.html.twig',[
            'pager' => $pager,
            'search_form' => $searchForm->createView(),
            'min' => $min,
            'max' => ($min == $max) ? $min + 1 : $max
        ]));
    }

    /**
     * @Route("/{sku}", name="web_shop_show")
     */
    public function show(Request $request)
    {
        $item = $this->variationRepository->findOne($request->attributes->get('sku'))->getQuery()->getOneOrNullResult();
        return new Response($this->engine->render('shop/show/page.html.twig',[
            'item' => $item
        ]));        
    }
}