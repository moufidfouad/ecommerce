<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Entity\Variation;
use App\Entity\Order\Cart;
use App\Gateway\CartManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * @Route("/cart")
 */
class CartController //extends AbstractController
{
    /** @var CartManagerInterface */
    private $cartManager;
    /** @var Environment $engine */
    private $engine;
    /** @var RouterInterface $router */
    private $router;
    public function __construct(
        CartManagerInterface $cartManager,
        Environment $engine,
        RouterInterface $router
    )
    {
        $this->cartManager = $cartManager;
        $this->engine = $engine;
        $this->router = $router;
    }


    /**
     * @Route("", name="web_cart_index")
     */
    public function index()
    {
        $cart = $this->cartManager->getCart();
        $count = $cart instanceof Cart && !$cart->getItems()->isEmpty();
        if($count){
            return new Response($this->engine->render('shop/cart/index/page.html.twig',[
                'cart' => $cart
            ]));   
        } 
        
        return new RedirectResponse($this->router->generate('web_shop_index'));
    }

    /**
     * @Route("/add/{sku}", name="web_cart_add", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function add(Variation $variation,Request $request)
    {
        $form = $this->cartManager->createFormForAddItemInCart($variation,'web_cart_add',[
            'sku' => $variation->getSku()
        ]);

        $cart = $this->cartManager->getCart();
        $result = $this->cartManager->addInCart($form,$request);
        $key = $result ? 'success' : 'error';
        return $this->getJsonResponse($variation,$cart,'web_cart_add',$key);
    } 

    /**
     * @Route("/edit/{sku}", name="web_cart_edit", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function edit(Variation $variation,Request $request)
    {
        $form = $this->cartManager->createFormForEditItemInCart($variation,'web_cart_edit',[
            'sku' => $variation->getSku()
        ]);
        $cart = $this->cartManager->getCart();
        $result = $this->cartManager->editInCart($form,$request);
        $key = $result === true ? 'success' : 'error';
        return $this->getJsonResponse($variation,$cart,'web_cart_edit',$key);
    }
    
    /**
     * @Route("/delete/{sku}", name="web_cart_delete", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function delete(Variation $variation,Request $request)
    {
        $form = $this->cartManager->createFormForDeleteItemInCart($variation,'web_cart_delete',[
            'sku' => $variation->getSku()
        ]);
        $cart = $this->cartManager->getCart();
        $result = $this->cartManager->deleteInCart($form,$request);
        $key = $result === true ? 'success' : 'error';
        return $this->getJsonResponse($variation,$cart,'web_cart_delete',$key);
    }

    private function getJsonResponse(Variation $variation,Cart $cart,string $route,string $key)
    {
        $message = [
            'type' => $key,
            'message' => $this->cartManager->buildMessage($route,$key)
        ];
        return new JsonResponse([
            'message' => $message,
            'id' => $variation->getId(),
            'content' => [
                'cart' => [
                    'header' => $this->engine->render('shop/cart/includes/_head.html.twig', [
                        'cart' => $cart
                    ]),
                    'forms' => $this->engine->render('shop/cart/includes/_forms.html.twig', [
                        'produit' => $variation,
                        'cart' => $cart
                    ])
                ]
            ]
        ]);        
    }
}