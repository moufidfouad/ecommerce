<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Entity\Order\Cart;
use App\Gateway\CartManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("/checkout")
 */
class CheckoutController
{
    /** @var CartManagerInterface */
    private $cartManager;
    /** @var Environment $engine */
    private $engine;
    /** @var RouterInterface $router */
    private $router;
    public function __construct(
        CartManagerInterface $cartManager,
        Environment $engine,
        RouterInterface $router
    )
    {
        $this->cartManager = $cartManager;
        $this->engine = $engine;
        $this->router = $router;
    }

    /**
     * @Route("", name="web_checkout_index")
     */
    public function index(): Response
    {
        $cart = $this->cartManager->getCart();
        $count = $cart instanceof Cart && !$cart->getItems()->isEmpty();
        if($count){
            return new Response($this->engine->render('shop/checkout/index/page.html.twig', [
                'cart' => $cart
            ]));
        }

        return new RedirectResponse($this->router->generate('web_shop_index'));
    }
}
