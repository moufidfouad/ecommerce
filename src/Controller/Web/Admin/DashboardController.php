<?php

namespace App\Controller\Web\Admin;

use App\Entity\User;
use App\Entity\Rayon;
use App\Entity\Promotion;
use App\Entity\PromotionItem;
use App\Entity\Stock;
use App\Entity\Marque;
use App\Entity\Lieu;
use App\Entity\Couleur;
use App\Entity\Produit;
use App\Entity\Variation;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{    
    /**
     * @Route("/admin", name="web_dashboard")
     */    
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<b>Ecom</b>')
            ->setFaviconPath('img/favicon.png')
        ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return UserMenu::new()
            ->displayUserName()
            ->displayUserAvatar(false)
            ->setName($user instanceof User ? (string) $user->getFullname() : null)
            ->setAvatarUrl(null)
            ->setMenuItems([
                MenuItem::linkToLogout('security.profile.logout','fa fa-fw fa-lock')
            ])
        ;
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linktoDashboard('menu.homepage', 'fa fa-home'),

            MenuItem::subMenu('entity.variation.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.variation.list.children.list','fa fa-th-list',Variation::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.variation.list.children.create','fa fa-plus',Variation::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.stock.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.stock.list.children.list','fa fa-th-list',Stock::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.stock.list.children.create','fa fa-plus',Stock::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.promotion.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.promotion_item.list.children.list','fa fa-th-list',PromotionItem::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.promotion.list.children.list','fa fa-th-list',Promotion::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.promotion.list.children.create','fa fa-plus',Promotion::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.produit.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.produit.list.children.list','fa fa-th-list',Produit::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.produit.list.children.create','fa fa-plus',Produit::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.rayon.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.rayon.list.children.list','fa fa-th-list',Rayon::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.rayon.list.children.create','fa fa-plus',Rayon::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.marque.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.marque.list.children.list','fa fa-th-list',Marque::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.marque.list.children.create','fa fa-plus',Marque::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.couleur.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.couleur.list.children.list','fa fa-th-list',Couleur::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.couleur.list.children.create','fa fa-plus',Couleur::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.lieu.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.lieu.list.children.list','fa fa-th-list',Lieu::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.lieu.list.children.create','fa fa-plus',Lieu::class)
                    ->setQueryParameter('crudAction','new')
            ]),
        ];
    }
}
