<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\Marque;
use App\Controller\Traits\AdminCrudTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MarqueCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return Marque::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.marque.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.marque.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.marque.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.marque.show.title')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->disable(Action::DETAIL)
            ->add(Crud::PAGE_INDEX,Action::DETAIL)
            //->add(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_DETAIL, Action::INDEX)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('valeur','entity.marque.form.fields.valeur'),
        ];
    }
}