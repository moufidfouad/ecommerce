<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\Lieu;
use Doctrine\ORM\QueryBuilder;
use App\Repository\LieuRepository;
use App\Controller\Traits\AdminCrudTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LieuCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return Lieu::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.lieu.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.lieu.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.lieu.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.lieu.show.title')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->disable(Action::DETAIL)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('titre','entity.lieu.form.fields.titre'),

            TextField::new('proprietaire','entity.lieu.form.fields.proprietaire'),

            TextField::new('adresse','entity.lieu.form.fields.adresse'),

            EmailField::new('email','entity.lieu.form.fields.email'),

            TextField::new('telephone','entity.lieu.form.fields.telephone'),
        ];
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto,$entityDto,$fields,$filters);
        return $this->getFromRepository(function(LieuRepository $repository) use ($qb){
            return $repository->getList($qb);
        });
    }
}