<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\Produit;
use Doctrine\ORM\QueryBuilder;
use App\Repository\ProduitRepository;
use App\Controller\Traits\AdminCrudTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProduitCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return Produit::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.produit.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.produit.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.produit.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.produit.show.title')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->disable(Action::DETAIL)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('titre','entity.produit.form.fields.titre'),

            AssociationField::new('variations','entity.produit.list.fields.variations')
                ->hideOnForm(),
        ];
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto,$entityDto,$fields,$filters);
        return $this->getFromRepository(function(ProduitRepository $repository) use ($qb){
            return $repository->getList($qb,[
                sprintf('%s.variations',$qb->getRootAlias()) => 'variations'
            ]);
        });
    }
}