<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\Promotion;
use App\Form\Type\PromotionItemType;
use Doctrine\ORM\QueryBuilder;
use App\Repository\PromotionRepository;
use App\Controller\Traits\AdminCrudTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;


class PromotionCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return Promotion::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.promotion.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.promotion.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.promotion.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.promotion.show.title')
            ->setFormThemes([
                '@EasyAdmin/crud/form_theme.html.twig',
                'themes/jquery.collection.html.twig',
                'admin/promotion/includes/_item_form_theme.html.twig'                
            ])
            ->overrideTemplates([
                'crud/new' => 'admin/promotion/new.html.twig',
                'crud/edit' => 'admin/promotion/edit.html.twig'
            ])
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [ 
            TextField::new('titre','entity.promotion.form.fields.titre'),

            DateTimeField::new('debut','entity.promotion.form.fields.debut'),

            DateTimeField::new('fin','entity.promotion.form.fields.fin'),

            CollectionField::new('items','entity.promotion.form.fields.items')
                ->setFormTypeOptions([
                    'entry_type' => PromotionItemType::class,
                    'attr' => [
                        'class' => 'table items-collection'
                    ],
                    'by_reference' => false, 
                    'allow_add' => true, 
                    'allow_delete' => true, 
                    'delete_empty' => true,
                    'entry_options'  => [
                        'label' => false,
                        'attr'  => [
                            'class' => 'item-item'
                        ]
                    ]
                ])
                ->setTemplatePath('admin/promotion/fields/items.html.twig')
        ];
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto,$entityDto,$fields,$filters);
        return $this->getFromRepository(function(PromotionRepository $repository) use ($qb){
            return $repository->getList($qb,[
                sprintf('%s.items',$qb->getRootAlias()) => 'items',
                'items.produit' => 'variation'
            ]);
        });
    }
}