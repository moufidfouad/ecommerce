<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\Rayon;
use Doctrine\ORM\QueryBuilder;
use App\Form\Field\Rayon\UniteField;
use App\Controller\Traits\AdminCrudTrait;
use App\Repository\RayonRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RayonCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return Rayon::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.rayon.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.rayon.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.rayon.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.rayon.show.title')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->disable(Action::DETAIL)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('titre','entity.rayon.form.fields.titre'),

            UniteField::new('unites','entity.rayon.form.fields.unites')
                    ->setTemplatePath('admin/rayon/fields/unites.html.twig')
        ];
    }


    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto,$entityDto,$fields,$filters);
        return $this->getFromRepository(function(RayonRepository $repository) use ($qb){
            return $repository->getList($qb,[
                sprintf('%s.unites',$qb->getRootAlias()) => 'unites'
            ]);
        });
    }
}