<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\Stock;
use Doctrine\ORM\QueryBuilder;
use App\Form\Entity\LieuEntityType;
use App\Repository\StockRepository;
use App\Controller\Traits\AdminCrudTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StockCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return Stock::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.stock.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.stock.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.stock.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.stock.show.title')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->disable(Action::DETAIL)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [ 
            TextField::new('titre','entity.stock.form.fields.titre'),


            AssociationField::new('lieu','entity.stock.form.fields.lieu')
                    ->setFormType(LieuEntityType::class)
                    ->setFormTypeOption('required',true)
                    ->setTemplatePath('admin/stock/fields/lieu.html.twig'),

            TextareaField::new('description','entity.stock.form.fields.description'),

            AssociationField::new('produits','entity.stock.list.fields.produits')
                ->hideOnForm(),
        ];
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto,$entityDto,$fields,$filters);
        return $this->getFromRepository(function(StockRepository $repository) use ($qb){
            return $repository->getList($qb,[
                sprintf('%s.lieu',$qb->getRootAlias()) => 'lieu',
                sprintf('%s.produits',$qb->getRootAlias()) => 'variations'
            ]);
        });
    }
}