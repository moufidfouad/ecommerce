<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\Variation;
use Doctrine\ORM\QueryBuilder;
use App\Form\Field\VichImageField;
use App\Form\Entity\StockEntityType;
use App\Form\Entity\UniteEntityType;
use App\Form\Entity\MarqueEntityType;
use App\Form\Entity\CouleurEntityType;
use App\Form\Entity\ProduitEntityType;
use App\Repository\VariationRepository;
use App\Controller\Traits\AdminCrudTrait;
use App\Form\Field\Variation\TailleField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class VariationCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return Variation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.variation.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.variation.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.variation.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.variation.show.title')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('stock','entity.variation.form.fields.stock')
                ->setFormType(StockEntityType::class)
                ->setFormTypeOption('required',true)
                ->setTemplatePath('admin/variation/fields/stock.html.twig')
                ->hideOnIndex(),

            AssociationField::new('produit','entity.variation.form.fields.produit')
                ->setFormType(ProduitEntityType::class)
                ->setFormTypeOption('required',true)
                ->setTemplatePath('admin/variation/fields/produit.html.twig'),

            AssociationField::new('marque','entity.variation.form.fields.marque')
                ->setFormType(MarqueEntityType::class)
                ->setTemplatePath('admin/variation/fields/marque.html.twig'),

            NumberField::new('prix','entity.variation.form.fields.prix'),

            TailleField::new('taille','entity.variation.form.fields.taille')
                ->setTemplatePath('admin/variation/fields/taille.html.twig'),

            NumberField::new('poids','entity.variation.form.fields.poids')
                ->hideOnIndex(),

            NumberField::new('quantite','entity.variation.form.fields.quantite'),

            AssociationField::new('unites','entity.variation.form.fields.unites')
                ->hideOnIndex()
                ->setFormType(UniteEntityType::class)
                ->setFormTypeOption('required',true)
                ->setTemplatePath('admin/variation/fields/unites.html.twig'),

            VichImageField::new('file','entity.variation.form.fields.file')
                ->setFormTypeOption('required',$pageName == Crud::PAGE_NEW)
                ->setTemplatePath('admin/variation/fields/image.html.twig'),

            AssociationField::new('couleurs','entity.variation.form.fields.couleurs')
                ->hideOnIndex()
                ->setFormType(CouleurEntityType::class)
                ->setFormTypeOptions([
                    'multiple' => true,
                    'expanded' => true
                ]),

            TextareaField::new('description','entity.variation.form.fields.description')
                ->hideOnIndex()
        ];
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto,$entityDto,$fields,$filters);
        return $this->getFromRepository(function(VariationRepository $repository) use ($qb){
            return $repository->getList($qb,[
                sprintf('%s.produit',$qb->getRootAlias()) => 'produit',
                sprintf('%s.marque',$qb->getRootAlias()) => 'marque',
                sprintf('%s.taille',$qb->getRootAlias()) => 'taille'
            ]);
        });
    }
}