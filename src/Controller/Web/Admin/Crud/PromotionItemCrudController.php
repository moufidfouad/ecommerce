<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\PromotionItem;
use Doctrine\ORM\QueryBuilder;
use App\Repository\PromotionItemRepository;
use App\Controller\Traits\AdminCrudTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;


class PromotionItemCrudController extends AbstractCrudController
{
    use AdminCrudTrait;

    public static function getEntityFqcn(): string
    {
        return PromotionItem::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.promotion_item.list.label')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->disable(Action::NEW,Action::EDIT)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [ 
            ImageField::new('image','entity.promotion_item.list.fields.image')
                ->setTemplatePath('admin/promotion_item/fields/image.html.twig'),

            AssociationField::new('produit','entity.promotion_item.form.fields.produit')
                ->setTemplatePath('admin/promotion_item/fields/produit.html.twig'),

            NumberField::new('prix','entity.promotion_item.form.fields.prix'),

            TextField::new('percentage','entity.promotion_item.list.fields.percentage')
                ->setTemplatePath('admin/promotion_item/fields/percentage.html.twig'),

            DateTimeField::new('endDiscount','entity.promotion_item.list.fields.endDiscount')
            ->setTemplatePath('admin/promotion_item/fields/fin.html.twig')
        ];
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto,$entityDto,$fields,$filters);
        return $this->getFromRepository(function(PromotionItemRepository $repository) use ($qb){
            return $repository->getList($qb,[
                sprintf('%s.produit',$qb->getRootAlias()) => 'variation'
            ]);
        });
    }
}