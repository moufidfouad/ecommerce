<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Entity\Client;
use App\Security\WebAuthenticator;
use App\Form\Type\RegistrationType;
use App\Gateway\UserGatewayInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

/**
 * @Route("/register")
 */
class RegistrationController
{
    /** @var FormFactoryInterface $form */
    private $form;
    /**@var RouterInterface $router */
    private $router;
    /**@var Environment $engine */
    private $engine;
    /** @var UserGatewayInterface $userGateway */
    private $userGateway;

    public function __construct(
        FormFactoryInterface $form,
        RouterInterface $router,
        Environment $engine,
        UserGatewayInterface $userGateway
    )
    {
        $this->form = $form;
        $this->router = $router;
        $this->engine = $engine;
        $this->userGateway = $userGateway;
    }

    /**
     * @Route("/", name="web_register")
     */
    public function register(Request $request): Response
    {
        $client = new Client();
        $form = $this->form->create(RegistrationType::class, $client,[
            'action' => $this->router->generate('web_register')
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $this->userGateway->fullfill($client,true);

            $this->userGateway->sendActivationToken(
                $client,
                $this->engine->render('emails/activation.html.twig',[
                    'token' => $client->getActivationToken()
                ])
            );

            $this->userGateway->save($client);
        
            return new RedirectResponse($this->router->generate('web_login'));
        }

        return new Response($this->engine->render('auth/registration/register.html.twig', [
            'form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/complete/{token}", name="web_register_complete")
     */
    public function complete(Request $request): Response
    {
        $user = $this->userGateway->getUserByActivationToken($request);
        return new Response($this->engine->render('auth/registration/registerSuccess.html.twig',[
            'user' => $user
        ]));
    }

    /**
     * @Route("/activate/{token}", name="web_register_activate")
     */
    public function activate(Request $request,GuardAuthenticatorHandler $guard, WebAuthenticator $authenticator): Response
    {
        $client = $this->userGateway->activate($request);

        return $guard->authenticateUserAndHandleSuccess(
            $client,
            $request,
            $authenticator,
            'main'
        );
    }
}