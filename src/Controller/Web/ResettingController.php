<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Form\Type\ResettingType;
use App\Entity\Security\Reset;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * @Route("/reset")
 */
class ResettingController
{
    
    /** @var FormFactoryInterface $form */
    private $form;    
    /** @var RouterInterface $router */
    private $router;
    /** @var Environment $engine */
    private $engine;
    public function __construct(
        FormFactoryInterface $form,
        RouterInterface $router,
        Environment $engine
    ){
        $this->form = $form;
        $this->router = $router;
        $this->engine = $engine;
    }

    /**
     * @Route("/", name="web_reset")
     */
    public function reset(Request $request)
    {
        $reset = new Reset();
        $form = $this->form->create(ResettingType::class, $reset,[
            'action' => $this->router->generate('web_reset')
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            dd('send reset token');
        }

        return new Response($this->engine->render('auth/resetting/reset.html.twig', [
            'form' => $form->createView(),
        ]));
    }
}