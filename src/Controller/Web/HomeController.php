<?php

namespace App\Controller\Web;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class HomeController
{
    /** @var RouterInterface $router */
    private $router;
    /** @var Environment $engine */
    private $engine;
    public function __construct(
        RouterInterface $router,
        Environment $engine
    ){
        $this->router = $router;
        $this->engine = $engine;
    }

    /**
     * @Route("", name="web_index")
     */
    public function index()
    {
       return new RedirectResponse($this->router->generate('web_shop_index'));
    }
}