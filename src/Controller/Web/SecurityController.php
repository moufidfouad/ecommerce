<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Controller\Traits\UserControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SecurityController
{   
    use UserControllerTrait; 

    /** @var RouterInterface $router */
    private $router;
    /** @var Environment $engine */
    private $engine;
    public function __construct(
        TokenStorageInterface $token,
        RouterInterface $router,
        Environment $engine
    ){
        $this->token = $token;
        $this->router = $router;
        $this->engine = $engine;
    }

    /**
     * @Route("/login", name="web_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return new RedirectResponse($this->router->generate('web_index'));
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return new Response($this->engine->render('auth/login/login.html.twig', [
            'last_username' => $lastUsername, 
            'error' => $error
        ]));
    }

    /**
     * @Route("/logout", name="web_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}