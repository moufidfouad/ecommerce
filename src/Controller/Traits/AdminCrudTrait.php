<?php

namespace App\Controller\Traits;

trait AdminCrudTrait
{
    protected function getFromRepository(callable $callable,string $entity = null)
    {
        $class = is_null($entity) ? static::getEntityFqcn() : $entity;
        $repository = $this->getDoctrine()->getRepository($class);
        return call_user_func($callable,$repository);
    }    
}