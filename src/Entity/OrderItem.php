<?php

namespace App\Entity;

use App\Repository\OrderItemRepository;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemRepository::class)
 */
class OrderItem
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Variation::class, inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $panier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?Variation
    {
        return $this->article;
    }

    public function setArticle(?Variation $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getPanier(): ?Order
    {
        return $this->panier;
    }

    public function setPanier(?Order $panier): self
    {
        $this->panier = $panier;

        return $this;
    }

    public function equals(OrderItem $item): bool
    {
        return $this->getArticle()->getId() === $item->getArticle()->getId();
    }

    public function getTotal(): float
    {
        return $this->getArticle()->getPrix() * $this->getQuantite();
    }
}
