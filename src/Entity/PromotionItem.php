<?php

namespace App\Entity;

use App\Repository\PromotionItemRepository;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=PromotionItemRepository::class)
 */
class PromotionItem
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Variation::class, inversedBy="promotions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity=Promotion::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $promotion;

    public function validate(ExecutionContextInterface $context,$payload)
    {
        if($this->prix >= $this->produit->getPrix()){
            $context
                ->buildViolation('promotion_item.prix.max')
                ->atPath('prix')
                ->addViolation()
            ;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduit(): ?Variation
    {
        return $this->produit;
    }

    public function setProduit(?Variation $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getPromotion(): ?Promotion
    {
        return $this->promotion;
    }

    public function setPromotion(?Promotion $promotion): self
    {
        $this->promotion = $promotion;

        return $this;
    }
}
