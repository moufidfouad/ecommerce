<?php 

namespace App\Entity\Order;

use App\Entity\Variation;

class Item
{
    /** @var Cart */
    private $cart;
    /** @var Variation */
    private $variation;
    /** @var int */
    private $quantite;

    public function getVariation(): ?Variation
    {
        return $this->variation;
    }

    public function setVariation(Variation $variation): void
    {
        $this->variation = $variation;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): void
    {
        $this->quantite = $quantite;
    }

    public function getPrix(): ?float
    {
        return $this->variation->getRelativePrice() * $this->quantite;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }
}