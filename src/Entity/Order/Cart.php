<?php 

namespace App\Entity\Order;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use LogicException;

class Cart
{
    const SESSION_CART_NAME = 'cart';
    const TAX_RATE = 10; 
    /**
     * @var Collection|Item[]
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    public function addItem(Item $item): self
    {
        if($this->items->exists(function($i,Item $it) use ($item){
            return $it->getVariation()->getId() === $item->getVariation()->getId();
        })){
            $existItem = $this->items[$item->getVariation()->getId()];
            $quantite = $existItem->getQuantite();
            $existItem->setQuantite($quantite + $item->getQuantite()); 
        }else{ 
            $this->items[$item->getVariation()->getId()] = $item;
            $item->setCart($this);                    
        }

        return $this;
    }

    public function editItem(Item $item): self
    {
        $filter = $this->items->filter(function(Item $it) use ($item){
            return $it->getVariation()->getId() === $item->getVariation()->getId();
        });
        if($filter->count() != 1){
            throw new LogicException();
        }
        $filter->first()->setQuantite($item->getQuantite());

        return $this;
    }

    public function removeItem(Item $item): self
    {
        $filter = $this->items->filter(function(Item $it) use ($item){
            return $it->getVariation()->getId() === $item->getVariation()->getId();
        });

        if($filter->count() != 1){
            throw new LogicException();
        }

        $this->items->removeElement($item);
        if ($item->getCart() === $this) {
            $item->setCart(null);
        }
        unset($item);

        return $this;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getPrix(): ?float
    {
        $prix = 0;
        foreach($this->items as $item){
            $prix = $prix + $item->getPrix();
        }

        return $prix;
    }

    public function getShippement()
    {
        return round($this->getPrix() * (self::TAX_RATE / 100));
    }

    public function getTotal()
    {
        return $this->getPrix() + $this->getShippement();
    }
}