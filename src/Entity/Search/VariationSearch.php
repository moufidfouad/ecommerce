<?php

namespace App\Entity\Search;

use App\Entity\Unite;
use App\Entity\Marque;
use App\Entity\Taille;
use App\Entity\Couleur;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class VariationSearch
{
    public const ORDER_PRICE_TO_LOWER = 'PRICE_TO_LOWER';
    public const ORDER_PRICE_TO_HIGHER = 'PRICE_TO_HIGHER';
    public static $ORDERS_CHOICES = [
        'entity.variation.search.fields.order.name' => null,
        'entity.variation.search.fields.order.choices.prix.asc' => self::ORDER_PRICE_TO_LOWER,
        'entity.variation.search.fields.order.choices.prix.desc' => self::ORDER_PRICE_TO_HIGHER
    ];

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var null|string
     */
    private $titre;

    /**
     * @var null|string
     */
    private $order;
	
    /**
     * @var null|float
     */
    private $min;
	
    /**
     * @var null|float
     */
    private $max;   
    
    /**
     * @var null|bool
     */
    private $isDiscount;

    /**
     * @var Collection|Unite[]
     */
    private $unites;

    /**
     * @var Collection|Marque[]
     */
    private $marques;

    /**
     * @var Collection|Couleur[]
     */
    private $couleurs;

    /**
     * @var Collection|Taille[]
     */
    private $tailles;

    public function __construct()
    {        
        $this->page = 1;
        $this->limit = 3;
        $this->couleurs = new ArrayCollection();
        $this->unites = new ArrayCollection();
        $this->marques = new ArrayCollection();
        $this->tailles = new ArrayCollection();
    }

    /**
     * Get the value of page
     *
     * @return  int
     */ 
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set the value of page
     *
     * @param  int  $page
     *
     * @return  self
     */ 
    public function setPage(int $page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get the value of titre
     *
     * @return  null|string
     */ 
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set the value of titre
     *
     * @param  null|string  $titre
     *
     * @return  self
     */ 
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get the value of order
     *
     * @return  null|string
     */ 
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set the value of order
     *
     * @param  null|string  $order
     *
     * @return  self
     */ 
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get the value of min
     *
     * @return  null|float
     */ 
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set the value of min
     *
     * @param  null|float  $min
     *
     * @return  self
     */ 
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get the value of max
     *
     * @return  null|float
     */ 
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Set the value of max
     *
     * @param  null|float  $max
     *
     * @return  self
     */ 
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get the value of isDiscount
     *
     * @return  null|bool
     */ 
    public function getIsDiscount()
    {
        return $this->isDiscount;
    }

    /**
     * Set the value of isDiscount
     *
     * @param  null|bool  $isDiscount
     *
     * @return  self
     */ 
    public function setIsDiscount($isDiscount)
    {
        $this->isDiscount = $isDiscount;

        return $this;
    }

    /**
     * @return Collection|Unite[]
     */
    public function getUnites(): Collection
    {
        return $this->unites;
    }

    public function addUnite(Unite $unite): self
    {
        if (!$this->unites->contains($unite)) {
            $this->unites[] = $unite;
        }

        return $this;
    }

    public function removeUnite(Unite $unite): self
    {
        if ($this->unites->contains($unite)) {
            $this->unites->removeElement($unite);
        }

        return $this;
    }

    /**
     * @return Collection|Couleur[]
     */
    public function getCouleurs(): Collection
    {
        return $this->couleurs;
    }

    public function addCouleur(Couleur $couleur): self
    {
        if (!$this->couleurs->contains($couleur)) {
            $this->couleurs[] = $couleur;
        }

        return $this;
    }

    public function removeCouleur(Couleur $couleur): self
    {
        if ($this->couleurs->contains($couleur)) {
            $this->couleurs->removeElement($couleur);
        }

        return $this;
    }

    /**
     * @return Collection|Marque[]
     */
    public function getMarques(): Collection
    {
        return $this->marques;
    }

    public function addMarque(Marque $marque): self
    {
        if (!$this->marques->contains($marque)) {
            $this->marques[] = $marque;
        }

        return $this;
    }

    public function removeMarque(Marque $marque): self
    {
        if ($this->marques->contains($marque)) {
            $this->marques->removeElement($marque);
        }

        return $this;
    }

    /**
     * @return Collection|Taille[]
     */
    public function getTailles(): Collection
    {
        return $this->tailles;
    }

    public function addTaille(Taille $taille): self
    {
        if (!$this->tailles->contains($taille)) {
            $this->tailles[] = $taille;
        }

        return $this;
    }

    public function removeTaille(Taille $taille): self
    {
        if ($this->tailles->contains($taille)) {
            $this->tailles->removeElement($taille);
        }

        return $this;
    }

    /**
     * Get the value of limit
     *
     * @return  int
     */ 
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set the value of limit
     *
     * @param  int  $limit
     *
     * @return  self
     */ 
    public function setLimit(int $limit)
    {
        $this->limit = $limit;

        return $this;
    }
}