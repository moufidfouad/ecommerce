<?php

namespace App\Entity\Traits;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use LogicException;
use Symfony\Component\Validator\Constraints as Assert;

trait DiscountableTrait
{
    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Positive()
     */
    private $discount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDiscount;

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getEndDiscount(): ?\DateTimeInterface
    {
        return $this->endDiscount;
    }

    public function setEndDiscount(?\DateTimeInterface $endDiscount): self
    {
        $this->endDiscount = $endDiscount;

        return $this;
    }

    public function inDiscount(\DateTimeInterface $date = null)
    {
        is_null($date) ? $date = new DateTime() : $date;
        return $this->endDiscount > $date;
    }

    abstract function getRegularPrice();

    public function getRelativePrice(\DateTimeInterface $date = null)
    {
        if($this->inDiscount()){
            if(is_null($this->discount)){
                throw new LogicException("No discount price provided");
            }
            return $this->discount;
        }

        return $this->getRegularPrice();
    }

    public function getPercent(DateTimeInterface $date = null)
    {
        if($this->inDiscount($date)){
            return (($this->getRegularPrice() - $this->getDiscount()) / $this->getRegularPrice()) * 100;
        }
    }
}