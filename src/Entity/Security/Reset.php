<?php

namespace App\Entity\Security;

class Reset
{
    /** 
     * @var string|null $username 
     */
    private $username;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }
}