<?php

namespace App\Entity\Security;

use App\Validator\OldPassword;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

class ChangePassword
{
    /**
     * @OldPassword(message="user.password.not_old")
     * @Serializer\Groups({"read"})
     */
    private $old;

    /**
     * @Assert\NotBlank(message="user.password.blank")
     * @Assert\Length(min=100,minMessage="user.password.min")
     * @Serializer\Groups({"read"})
     */
    private $new;

    /**
     * @return mixed
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * @param mixed $new
     */
    public function setNew($new): void
    {
        $this->new = $new;
    }

    public function getOld(): ?string
    {
        return $this->old;
    }

    public function setOld(string $old): self
    {
        $this->old = $old;

        return $this;
    }    
}