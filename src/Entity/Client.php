<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */ 
class Client extends User
{
    /**
     * @ORM\Column(type="string", length=50)
     */
     private $nom;

     /**
      * @ORM\Column(type="string", length=50)
      */
     private $prenom;

    public function __construct()
    {
        $this->enabled = false;
        $this->addRole(static::ROLE_DEFAULT);
    }
 
    public function getFullname(): ?string
    {
        return '';
    }
 
    public function getNom(): ?string
    {
        return $this->nom;
    }
 
    public function setNom(?string $nom): self
    {
        $this->nom = $nom ? ucfirst($nom) : $nom;
 
        return $this;
    }
 
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }
 
    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom ? ucfirst($prenom) : $prenom;
 
        return $this;
    }
}
