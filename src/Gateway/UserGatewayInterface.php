<?php

namespace App\Gateway;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;

interface UserGatewayInterface
{
    function fullfill(User $user,bool $with_token = false);
    function sendActivationToken(User $user,string $twig);
    function save(User $user);
    function getUserByActivationToken(Request $request);
    function activate(Request $request);
}