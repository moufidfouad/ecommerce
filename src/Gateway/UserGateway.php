<?php

namespace App\Gateway;

use App\Entity\User;
use App\Library\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserGateway implements UserGatewayInterface
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;
    /** @var TranslatorInterface $translator */
    private $translator;

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder,
        TranslatorInterface $translator
    )
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->translator = $translator;
    }

    public function fullfill(User $user,bool $with_token = false)
    {
        $user->setPassword(
            $this->encoder->encodePassword(
                $user,
                $user->getPlainPassword()
            )
        );  
        
        $user->setActivationToken($with_token ? Tools::getToken(10) : null);
    }

    public function sendActivationToken(User $user,string $twig)
    {
        dd('send activation account');
        /*$envelope = $this->messageBus->dispatch(
            new Mail(
                $this->translator->trans('Account activation',[],'security'),
                new Address(
                    'email@no-reply.com',
                    $this->translator->trans('site.name')
                ),
                $user->getUsername(),
                $twig
            )
        );*/
    }

    public function save(User $user)
    {
        if(!$user->getId()){
            $this->em->persist($user);
        }        
        $this->em->flush();
    }

    public function getUserByActivationToken(Request $request)
    {
        $entity = $this->em->getRepository(User::class)->findOneBy([
            'activation_token' => $request->attributes->get('token')
        ]);

        if(!is_null($entity)){
            return $entity;
        }
        
        throw new NotFoundHttpException('Lien incorrect');
    }
    public function activate(Request $request)
    {
        $entity = $this->getUserByActivationToken($request);

        $entity->setActivationToken(null);
        $entity->setEnabled(true);
        $this->em->flush();

        return $entity;
    }
}