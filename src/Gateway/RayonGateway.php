<?php

namespace App\Gateway;

use App\Entity\Rayon;
use App\Entity\Filters\RayonFilter;
use App\Repository\RayonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RayonGateway implements RayonGatewayInterface
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var RayonRepository $rayonRepository */
    private $rayonRepository;
    /** @var PaginatorInterface $paginator */
    private $paginator;
    /** @var ParameterBagInterface $bag */
    private $bag;

    public function __construct(
        EntityManagerInterface $em,
        PaginatorInterface $paginator,
        ParameterBagInterface $bag
    )
    {
        $this->em = $em;
        $this->rayonRepository = $em->getRepository(Rayon::class);
        $this->paginator = $paginator;
        $this->bag = $bag;
    }


    public function save(Rayon $rayon)
    {
        if(!$rayon->getId()){
            $this->em->persist($rayon);
        }        
        $this->em->flush();
    }

    public function getOne(Request $request,bool $withUnites = false)
    {
        $joins = !$withUnites ? [] : [
            'rayon.unites' => 'unites'
        ];

        $rayon = $this->rayonRepository
                ->getOne($request->attributes->get('slug'),$joins)
                ->getQuery()
                ->getOneOrNullResult()
        ;


        if(!is_null($rayon)){
            return $rayon;
        }
        
        throw new NotFoundHttpException('Lien incorrect');
    }

    public function getAll(bool $withUnites = false,RayonFilter $filter = null)
    {
        return $this->findAll($withUnites,$filter)
                ->getQuery()
                ->getResult()
        ;
    }

    public function getPaginator(Request $request,RayonFilter $filter = null,int $count = null)
    {
        $query = $this->findAll(true,$filter)->getQuery();
        $pagination = $this->paginator->paginate($query,
            $request->query->getInt('page', 1),
            is_null($count) ? $this->bag->get('list_count') : $count
        );

        return $pagination;
    }

    public function delete(Rayon $rayon)
    {
        $this->em->remove($rayon);
        $this->em->flush();
    }


    private function findAll(bool $withUnites = false,RayonFilter $filter = null)
    {
        $joins = !$withUnites ? [] : [
            'rayon.unites' => 'unites'
        ];

        return $this->rayonRepository
                ->getAll($joins,$filter)
        ;        
    }
}