<?php

namespace App\Gateway;

use App\Entity\Order\Cart;
use App\Entity\Variation;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

interface CartManagerInterface
{
    function addInCart(FormInterface $form,Request $request);
    function editInCart(FormInterface $form,Request $request);
    function deleteInCart(FormInterface $form,Request $request);
    function getCart(bool $strict = false);
    //function spendCart(Cart $cart = null);
    function removeCart();
    function createFormForAddItemInCart(Variation $variation,string $route,array $routeParams,array $formOptions = []);
    function createFormForEditItemInCart(Variation $variation,string $route,array $routeParams,array $formOptions = []);
    function createFormForDeleteItemInCart(Variation $variation,string $route,array $routeParams);
    function buildMessage(string $route,string $key);
}
