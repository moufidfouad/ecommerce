<?php

namespace App\Gateway;

use App\Entity\Order\Cart;
use App\Entity\Order\Item;
use App\Entity\Variation;
use App\Form\Type\ItemType;
use LogicException;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class CartManager implements CartManagerInterface
{
    /** @var FormFactoryInterface */
    private $form;
    /** @var RouterInterface */
    private $router;
    /** @var SessionInterface */
    private $session;
    /** @var TranslatorInterface */
    private $translator;

    public function __construct(
        FormFactoryInterface $form,
        Environment $twig,
        RouterInterface $router,
        SessionInterface $session,
        TranslatorInterface $translator
    )
    {
        $this->form = $form;
        $this->twig = $twig;
        $this->router = $router;
        $this->session = $session;
        $this->translator = $translator;
    }

    public function addInCart(FormInterface $form,Request $request)
    {
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $panier = $this->getCart();
            $panier->addItem($form->getData());
            $this->session->set(Cart::SESSION_CART_NAME,$panier);  

            return true;
        }

        return false;
    } 
    
    public function editInCart(FormInterface $form,Request $request)
    {
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $panier = $this->getCart(true);
            $panier->editItem($form->getData());
            $this->session->set(Cart::SESSION_CART_NAME,$panier);  

            return true;
        }

        return false;
    }

    public function deleteInCart(FormInterface $form,Request $request)
    {
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $panier = $this->getCart(true);
            $panier->removeItem($form->getData());
            $this->session->set(Cart::SESSION_CART_NAME,$panier);  

            return true;
        }

        return false;
    }

    public function getCart(bool $strict = false)
    {
        $cart = $this->session->get(Cart::SESSION_CART_NAME,$strict ? null : new Cart());
        if($strict == true && is_null($cart)){
            throw new LogicException('No cart in session');
        }
        return $cart;
    }

    public function removeCart()
    {
        $this->session->remove(Cart::SESSION_CART_NAME);
    }
    
    public function createFormForAddItemInCart(Variation $variation,string $route,array $routeParams,array $formOptions = [])
    {
        $item = new Item();
        $item->setVariation($variation);
        $form = $this->form->createNamed(sprintf('%s%s','add_item',$variation->getId()),ItemType::class,$item,[
            'max' => $variation->getQuantite(),
            'action' => $this->router->generate($route,$routeParams)
        ] + $formOptions);  
        
        return $form;
    }

    public function createFormForEditItemInCart(Variation $variation,string $route,array $routeParams,array $formOptions = [])
    {
        $cart = $this->getCart(true);
        $item = $cart->getItems()->get($variation->getId());

        $form = $this->form->createNamed(sprintf('%s%s','edit_item',$variation->getId()),ItemType::class,$item,[
            'max' => $variation->getQuantite(),
            'action' => $this->router->generate($route,$routeParams)
        ] + $formOptions);  
        
        return $form;
    }

    public function createFormForDeleteItemInCart(Variation $variation,string $route,array $routeParams)
    {
        $cart = $this->getCart(true);
        $item = $cart->getItems()->get($variation->getId());

        $form = $this->form->createNamed(sprintf('%s%s','delete_item',$variation->getId()),FormType::class,$item,[
            'action' => $this->router->generate($route,$routeParams),
            'attr' => [
                'id' => sprintf('%s%s','delete_item',$variation->getId())
            ]
        ]);  
        
        return $form;
    }

    public function buildMessage(string $route,string $key)
    {
        if($key === 'success'){
            switch($route){
                case 'web_cart_add':
                    return $this->translator->trans('entity.item.add.message.success');
                case 'web_cart_edit':
                    return $this->translator->trans('entity.item.edit.message.success');
                case 'web_cart_delete':
                    return $this->translator->trans('entity.item.delete.message.success');
            }
        }

        if($key === 'error'){
            switch($route){
                case 'web_cart_add':
                    return $this->translator->trans('entity.item.add.message.error');
                case 'web_cart_edit':
                    return $this->translator->trans('entity.item.edit.message.error');
                case 'web_cart_delete':
                    return $this->translator->trans('entity.item.delete.message.error');
            }
        }
    }
}
