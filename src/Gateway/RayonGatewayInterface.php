<?php

namespace App\Gateway;

use App\Entity\Rayon;
use App\Entity\Filters\RayonFilter;
use Symfony\Component\HttpFoundation\Request;

interface RayonGatewayInterface
{
    function save(Rayon $rayon);
    function getOne(Request $request,bool $withUnites = false);
    function getAll(bool $withUnites = false,RayonFilter $filter = null);
    function getPaginator(Request $request,RayonFilter $filter = null,int $count = null);
    function delete(Rayon $rayon);
}