<?php

namespace App\Validator;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UsernameExistsValidator extends ConstraintValidator
{
    /** @var UserRepository $userRepository */
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\UsernameExists */

        $user = $this->userRepository->findOneByUsernameOrEmail($value)->getQuery()->getOneOrNullResult();

        if(is_null($user)){
            $this->context->buildViolation($constraint->message)
            ->atPath('query')
            ->addViolation();
        }
    }
}
