<?php

namespace App\Twig;

use LogicException;
use Twig\TwigFilter;
use Twig\Environment;
use App\Library\Tools;
use Twig\Extension\AbstractExtension;
use Symfony\Contracts\Translation\TranslatorInterface;

class FilterExtension extends AbstractExtension
{
    /** @var TranslatorInterface $translator */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('timeDiff', [$this, 'timeDiff'], ['needs_environment' => true]),
            new TwigFilter('format_phone', [$this, 'phoneFormat']),
            new TwigFilter('format_number', [$this, 'numberFormat']),
            new TwigFilter('ile', [$this, 'getIle']),
        ];
    }

    /**
     * Time Ago.
     *
     * @return string
     */
    public function timeDiff(Environment $env, $date, $now = null, $text = 'diff.ago', $domain = 'messages', $length = 1): string
    {
        $units = [
            'y' => $this->translator->trans('diff.year', [], $domain),
            'm' => $this->translator->trans('diff.month', [], $domain),
            'd' => $this->translator->trans('diff.day', [], $domain),
            'h' => $this->translator->trans('diff.hour', [], $domain),
            'i' => $this->translator->trans('diff.minute', [], $domain),
            's' => $this->translator->trans('diff.second', [], $domain),
        ];

        // Date Time
        $date = twig_date_converter($env, $date);
        $now = twig_date_converter($env, $now);

        // Convert
        $diff = $date->diff($now);
        $format = '';

        $counter = 0;
        foreach ($units as $key => $val) {
            $count = $diff->$key;

            if (0 !== $count) {
                $format .= $count.' '.$val.' ';

                ++$counter;
                if ($counter === $length) {
                    break;
                }
            }
        }

        return $format ? $format.$this->translator->trans($text, [], $domain) : '';
    }

    /**
     * Phone Formatter.
     *
     * @return string
     */
    public function phoneFormat($phone): string
    {
        // Null | Empty | 0
        if (empty($phone) || 0 === $phone) {
            return '';
        }

        return mb_substr($phone, 0, 3).'-'.mb_substr($phone, 3, 3).'-'.mb_substr($phone, 6);
    }

    public function numberFormat($number,int $scale,$suffix = null)
    {
        if(!is_numeric($number)){
            throw new LogicException($number,implode(',',['int','float']));
        }

        return Tools::formatNumber($number,$scale,$suffix);
    }

    public function getIle(string $ile)
    {
        $choices = array_flip(Tools::$ILES_CHOICES);
        return array_key_exists($ile,$choices) ? $this->translator->trans($choices[$ile]) : $ile;
    }
}
