<?php

namespace App\Twig;

use App\Entity\Search\VariationSearch;
use LogicException;
use Twig\Environment;
use Twig\TwigFunction;
use App\Library\Tools;
use Twig\Extension\AbstractExtension;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FunctionExtension extends AbstractExtension
{
    /** @var ParameterBagInterface */
    private $bag;
    /** @var Environment */
    private $env;


    public function __construct(
        ParameterBagInterface $bag,
        Environment $env
    )
    {
        $this->bag = $bag;
        $this->env = $env;
    }

    /**
     * Create Twig Function.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('banner', [$this, 'renderBanner'], ['is_safe' => ['html']]),
            new TwigFunction('features', [$this, 'renderFeatures'], ['is_safe' => ['html']]),
            new TwigFunction('limit_selecter', [$this, 'getLimitSelecter'], ['is_safe' => ['html']]),
            new TwigFunction('order_selecter', [$this, 'getOrderSelecter'], ['is_safe' => ['html']]),
            new TwigFunction('title', [$this, 'title']),
            new TwigFunction('inArray', [$this, 'inArray']),
            new TwigFunction('pathInfo', [$this, 'pathInfo']),
        ];
    }

    public function renderBanner(string $page = '')
    {
        $options = [];
        if(!empty($page)){
            $options['_page'] = $page;
        }
        return $this->env->render('extensions/banner.html.twig',$options);
    }

    public function renderFeatures()
    {
        return $this->env->render('extensions/features.html.twig');
    }

    public function getLimitSelecter()
    {
        $array = Tools::getRangeNumber(3,18,3);
        return $this->env->render('extensions/limit_selecter.html.twig',[
            'array' => $array
        ]);
    }

    public function getOrderSelecter()
    {
        $array = VariationSearch::$ORDERS_CHOICES;
        return $this->env->render('extensions/order_selecter.html.twig',[
            'array' => $array
        ]);
    }

    /**
     * Return Panel Title.
     *
     * @param $title
     * @param bool $parent
     *
     * @return mixed
     */
    public function title($title, $parent = true)
    {
        return !$parent ? $title : str_replace(['&T', '&P'], [$title, $this->bag->get('head_title')], $this->bag->get('head_title_pattern'));
    }

    /**
     * Checks if a value exists in an array.
     *
     * @param $needle
     */
    public function inArray($needle, array $haystack): bool
    {
        return \in_array(mb_strtolower($needle), $haystack, false);
    }

    /**
     * Information about a file path.
     *
     * @param string $options
     */
    public function pathInfo(string $path, $options = 'extension'): string
    {
        return pathinfo($path)[mb_strtolower($options)];
    }
}
