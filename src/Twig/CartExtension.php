<?php

namespace App\Twig;

use Twig\Environment;
use Twig\TwigFunction;
use App\Entity\Variation;
use App\Gateway\CartManagerInterface;
use Twig\Extension\AbstractExtension;

class CartExtension extends AbstractExtension
{
    /** @var Environment */
    private $env;
    /** @var CartManagerInterface */
    private $cartManager;

    public function __construct(
        Environment $env,
        CartManagerInterface $cartManager
    )
    {
        $this->env = $env;
        $this->cartManager = $cartManager;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('cart_add_item', [$this, 'getFormAddItem'], ['is_safe' => ['html']]),
            new TwigFunction('cart_edit_item', [$this, 'getFormEditItem'], ['is_safe' => ['html']]),
            new TwigFunction('cart_delete_item', [$this, 'getFormDeleteItem'], ['is_safe' => ['html']]),
        ];
    }

    public function getFormAddItem(Variation $variation,string $routeAction,array $routeParams)
    {
        $form = $this->cartManager->createFormForAddItemInCart($variation,$routeAction,$routeParams); 
        
        return $this->env->render('extensions/cart/add_item.html.twig',[
            'form' => $form->createView()
        ]);
    }

    public function getFormEditItem(Variation $variation,string $editAction,array $editParams,string $deleteAction,array $deleteParams)
    {
        $editForm = $this->cartManager->createFormForEditItemInCart($variation,$editAction,$editParams); 
        
        return $this->env->render('extensions/cart/edit_item.html.twig',[
            'edit_form' => $editForm->createView(),
            'produit' => $variation,
            'delete_action' => $deleteAction,
            'delete_params' => $deleteParams
        ]);
    }

    public function getFormDeleteItem(Variation $variation,string $routeAction,array $routeParams)
    {
        $cart = $this->cartManager->getCart(true);
        $item = $cart->getItems()->get($variation->getId());
        $form = $this->cartManager->createFormForDeleteItemInCart($variation,$routeAction,$routeParams);
        
        return $this->env->render('extensions/cart/delete_item.html.twig',[
            'form' => $form->createView(),
            'item' => $item
        ]);
    }
}