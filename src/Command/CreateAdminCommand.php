<?php

namespace App\Command;

use App\Entity\Admin;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CreateAdminCommand extends Command
{
    protected static $defaultName = 'create:admin';
    /**@var EntityManagerInterface */
    private $em;
    /**@var UserPasswordEncoderInterface */
    private $encoder;
    /** @var ParameterBagInterface $bag */
    private $bag;
    public function __construct(
		EntityManagerInterface $em,
		UserPasswordEncoderInterface $encoder,
		ParameterBagInterface $bag
	)
    {
        parent::__construct();
        $this->em = $em;
        $this->encoder = $encoder;
        $this->bag = $bag;
    }
    
    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addOption('username', null, InputOption::VALUE_REQUIRED, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $username = $input->getOption('username');
        $admin = $this->em->getRepository(User::class)->findOneBy([
            'username' => $username
        ]);
        if (\is_null($admin)) {
            $admin = new Admin($username);
            $admin->setPassword($this->encoder->encodePassword($admin,$admin->getUsername()));
            $admin->setEmail($this->bag->get('contact_email'));
            try{
                $this->em->persist($admin);
                $this->em->flush();

                $io->success(sprintf('Le compte administrateur vient d\'être enregistré'));
            }catch(Exception $e){
                $io->note(sprintf($e->getMessage()));
            }            
        }else{
            $io->note(sprintf('Un compte administrateur a déjà été enregistré'));
        }        

        return 0;
    }
}
