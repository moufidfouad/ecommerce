<?php

namespace App\Command;

use App\Library\Tools;
use App\Entity\Couleur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FillCouleursCommand extends Command
{
    protected static $defaultName = 'fill:couleurs';
    /**@var EntityManagerInterface */
    private $em;
    public function __construct(
		EntityManagerInterface $em
	)
    {
        parent::__construct();
        $this->em = $em;
    }
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $colors = Tools::$COLOR_CHOICES;
        $couleurRepository = $this->em->getRepository(Couleur::class);
        $couleurs = $couleurRepository->findBy([
            'valeur' => $colors
        ]);

        $newValeurs = array_diff($colors,$couleurs);
        foreach($newValeurs as $valeur){
            $couleur = (new Couleur())->setValeur($valeur);
            $this->em->persist($couleur);
        }
        $this->em->flush();       
        $io->note(sprintf('Couleurs enregistrées'));
        return Command::SUCCESS;
    }
}
