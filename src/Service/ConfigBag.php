<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConfigBag
{
    /** @var ParameterBagInterface $bag */
    private $bag;
    public function __construct(ParameterBagInterface $bag)
    {
        $this->bag = $bag;
    }

    public function get(string $name)
    {
        // Load Symfony Parameters
        if ($this->bag->has($name)) {
            return $this->bag->get($name);
        }
        return null;
    }
}