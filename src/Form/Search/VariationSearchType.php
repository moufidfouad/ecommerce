<?php

namespace App\Form\Search;

use App\Form\Entity\UniteEntityType;
use App\Form\Entity\MarqueEntityType;
use App\Form\Entity\TailleEntityType;
use App\Entity\Search\VariationSearch;
use App\Repository\MarqueRepository;
use App\Repository\UniteRepository;
use App\Repository\TailleRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class VariationSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre',TextType::class,[
                'required' => false,
                'label' => false
            ])
            ->add('unites',UniteEntityType::class,[
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'label' => false,
                'query_builder' => function(UniteRepository $repository){
                    return $repository->findByQuantiteNotEmpty();
                }
            ])
            ->add('marques',MarqueEntityType::class,[
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'label' => false,
                'query_builder' => function(MarqueRepository $repository){
                    return $repository->findByQuantiteNotEmpty();
                }
            ])
            ->add('tailles',TailleEntityType::class,[
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'label' => false,
                'query_builder' => function(TailleRepository $repository){
                    return $repository->findByQuantiteNotEmpty();
                }
            ])
            ->add('min',NumberType::class,[
                'required' => false,
                'label' => false
            ])
            ->add('max',NumberType::class,[
                'required' => false,
                'label' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VariationSearch::class,
            'method' => 'GET',
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
