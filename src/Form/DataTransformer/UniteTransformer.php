<?php

namespace App\Form\DataTransformer;

use App\Entity\Unite;
use App\Repository\UniteRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\DataTransformerInterface;

class UniteTransformer implements DataTransformerInterface
{
    /**@var UniteRepository */
    private $uniteRepository;
    public function __construct(UniteRepository $uniteRepository)
    {
        $this->uniteRepository = $uniteRepository;
    }

    public function transform($value)
    {
        return implode(', ',$value);
    }

    public function reverseTransform($value)
    {
        $titres = array_unique(array_map(function($str){
            return $str;
        },array_filter(array_map('trim',explode(',',$value)))));

        $unites = $this->uniteRepository->findBy([
            'titre' => $titres
        ]);

        $newTitres = array_diff($titres,$unites);

        foreach($newTitres as $titre){
            $unite = new Unite();
            $unite->setTitre($titre);
            $unites[] = $unite;
        }
        return $unites;
    }
}