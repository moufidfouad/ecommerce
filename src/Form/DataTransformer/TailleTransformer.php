<?php

namespace App\Form\DataTransformer;

use App\Entity\Taille;
use App\Repository\TailleRepository;
use Symfony\Component\Form\DataTransformerInterface;

class TailleTransformer implements DataTransformerInterface
{
    /**@var TailleRepository */
    private $tailleRepository;
    /**@var callable */
    private $finderCallback;
    public function __construct(TailleRepository $tailleRepository, callable $finderCallback)
    {
        $this->tailleRepository = $tailleRepository;
        $this->finderCallback = $finderCallback;
    }
        
    public function transform($value)
    {
        return !is_null($value) ? $value->getValeur() : $value;         
    }

    public function reverseTransform($value)
    {
        if (!$value) {
            return;
        }

        $callback = $this->finderCallback;
        $entity = $callback($this->tailleRepository, $value);
        
        if(!is_null($entity)){
            return $entity;
        }
        $taille = new Taille();
        $taille->setValeur($value);
        return $taille;        
    }
}