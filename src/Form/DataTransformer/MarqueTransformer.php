<?php

namespace App\Form\DataTransformer;

use App\Entity\Marque;
use App\Repository\MarqueRepository;
use Symfony\Component\Form\DataTransformerInterface;

class MarqueTransformer implements DataTransformerInterface
{
    /**@var MarqueRepository */
    private $marqueRepository;
    /**@var callable */
    private $finderCallback;
    public function __construct(MarqueRepository $marqueRepository, callable $finderCallback)
    {
        $this->marqueRepository = $marqueRepository;
        $this->finderCallback = $finderCallback;
    }
        
    public function transform($value)
    {
        return !is_null($value) ? $value->getValeur() : $value;          
    }

    public function reverseTransform($value)
    {
        if (!$value) {
            return;
        }

        $callback = $this->finderCallback;
        $entity = $callback($this->marqueRepository, $value);
        
        if(!is_null($entity)){
            return $entity;
        }
        $marque = new Marque();
        $marque->setValeur($value);
        return $marque;        
    }
}