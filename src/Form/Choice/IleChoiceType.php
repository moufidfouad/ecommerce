<?php

namespace App\Form\Choice;

use App\Library\Tools;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class IleChoiceType extends ChoiceType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'choices' => array_flip(Tools::$ILES_CHOICES)
        ]);
    }   
}