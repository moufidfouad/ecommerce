<?php

namespace App\Form\Entity;

use App\Entity\Taille;
use App\Repository\TailleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TailleEntityType extends EntityType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Taille::class,
            'choice_label' => 'valeur',
            'query_builder' => function(TailleRepository $repository){
                $qb = $repository->createQueryBuilder('taille');
                $qb->orderBy('taille.createdAt','DESC');
                return $qb;
            }
        ]);
    }      
}