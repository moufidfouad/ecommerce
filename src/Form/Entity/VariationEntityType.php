<?php

namespace App\Form\Entity;

use App\Entity\Variation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Library\Tools;

class VariationEntityType extends EntityType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Variation::class,
            'choice_label' => function(Variation $variation){
                return sprintf('%s [%s]',$variation->getSku(),Tools::formatNumber($variation->getPrix(),0,'KMF'));
            }
        ]);
    }      
}