<?php

namespace App\Form\Entity;

use App\Entity\Stock;
use App\Repository\StockRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StockEntityType extends EntityType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Stock::class,
            'choice_label' => 'titre',
            'query_builder' => function(StockRepository $repository){
                $qb = $repository->createQueryBuilder('stock');
                $qb->orderBy('stock.createdAt','DESC');
                return $qb;
            }
        ]);
    }      
}