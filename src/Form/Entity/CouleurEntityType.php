<?php

namespace App\Form\Entity;

use App\Entity\Couleur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CouleurEntityType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Couleur::class,
            'label_html' => true,
            'choice_label' => function(Couleur $couleur){
                return sprintf('<span style="color:%s;background-color:%s"><i class="fa fa-circle"></i></span>',$couleur->getValeur(),$couleur->getValeur());
            }
        ]);
    } 
}