<?php

namespace App\Form\Entity;

use App\Entity\Marque;
use App\Repository\MarqueRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarqueEntityType extends EntityType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Marque::class,
            'choice_label' => 'valeur',
            'query_builder' => function(MarqueRepository $repository){
                $qb = $repository->createQueryBuilder('marque');
                $qb->orderBy('marque.createdAt','DESC');
                return $qb;
            }              
        ]);
    }      
}