<?php

namespace App\Form\Entity;

use App\Entity\Produit;
use App\Repository\ProduitRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitEntityType extends EntityType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Produit::class,
            'choice_label' => 'titre',
            'query_builder' => function(ProduitRepository $repository){
                $qb = $repository->createQueryBuilder('produit');
                $qb->orderBy('produit.createdAt','DESC');
                return $qb;
            }
        ]);
    }      
}