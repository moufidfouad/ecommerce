<?php

namespace App\Form\Entity;

use App\Entity\Lieu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LieuEntityType extends EntityType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Lieu::class,
            'choice_label' => function(Lieu $lieu){
                return sprintf('%s [%s]',$lieu->getTitre(),$lieu->getProprietaire());
            }
        ]);
    }      
}