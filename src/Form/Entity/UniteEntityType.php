<?php

namespace App\Form\Entity;

use App\Entity\Unite;
use App\Repository\UniteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniteEntityType extends EntityType 
{
    public function configureOptions(OptionsResolver $resolver)
    { 
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => Unite::class,
            'choice_label' => 'titre',
            'query_builder' => function(UniteRepository $repository){
                $qb = $repository->createQueryBuilder('unite');
                $qb->orderBy('unite.createdAt','DESC');
                return $qb;
            }
        ]);
    }      
}