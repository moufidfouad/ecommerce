<?php

namespace App\Form\Field\Rayon;

use App\Form\Type\UniteType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;

final class UniteField implements FieldInterface
{
    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setTemplateName('crud/field/text')
            ->setFormType(UniteType::class)
            ->addCssClass('field-text')
            ->setCustomOption(TextField::OPTION_MAX_LENGTH, null)
            ->setCustomOption(TextField::OPTION_RENDER_AS_HTML, false);
    }
}