<?php

namespace App\Form\Type;

use App\Repository\MarqueRepository;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use App\Form\DataTransformer\MarqueTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MarqueType extends AbstractType
{
    /**@var MarqueRepository */
    private $marqueRepository;
    public function __construct(MarqueRepository $marqueRepository)
    {
        $this->marqueRepository = $marqueRepository;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new MarqueTransformer($this->marqueRepository,$options['finder_callback']))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'finder_callback' => function(MarqueRepository $marqueRepository, string $valeur) {
                return $marqueRepository->findOneBy(['valeur' => $valeur]);
            }
        ]);
        $resolver->setAllowedTypes('finder_callback','callable');
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        $class = isset($attr['class']) ? $attr['class'].' ' : '';
        $class .= 'autocomplete-marque typeahead';
        $attr['class'] = $class;
        $view->vars['attr'] = $attr;
    }

    public function getParent()
    {
        return TextType::class;
    } 
}
