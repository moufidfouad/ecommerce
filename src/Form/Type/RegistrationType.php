<?php

namespace App\Form\Type;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username',EmailType::class,[
                'label' => false,
                'required' => true,
                'attr' => [
                    'placeholder' => 'security.register.username'
                ]
            ])
            ->add('nom',TextType::class,[
                'label' => false,
                'required' => true,
                'attr' => [
                    'placeholder' => 'security.register.nom'
                ]
            ])
            ->add('prenom',TextType::class,[
                'label' => false,
                'required' => true,
                'attr' => [
                    'placeholder' => 'security.register.prenom'
                ]
            ])  
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'first_options'  => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'security.register.password'
                    ]
                ],
                'second_options' => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'security.register.password_confirmation' 
                    ]
                ],
            ])          
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class            
        ]);
    }
}
