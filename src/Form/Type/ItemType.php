<?php

namespace App\Form\Type;

use App\Library\Tools;
use App\Entity\Order\Item;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite',ChoiceType::class,[
                'choices' => Tools::getRangeNumber(1,$options['max'])
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'max' => 1
        ]); 
        
        $resolver->setAllowedTypes('max','integer');
    }
}