<?php

namespace App\Form\Type;

use App\Repository\TailleRepository;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use App\Form\DataTransformer\TailleTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TailleType extends AbstractType
{
    /**@var TailleRepository */
    private $tailleRepository;
    public function __construct(TailleRepository $tailleRepository)
    {
        $this->tailleRepository = $tailleRepository;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new TailleTransformer($this->tailleRepository,$options['finder_callback']))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'finder_callback' => function(TailleRepository $tailleRepository, string $valeur) {
                return $tailleRepository->findOneBy(['valeur' => $valeur]);
            }
        ]);
        $resolver->setAllowedTypes('finder_callback','callable');
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        $class = isset($attr['class']) ? $attr['class'].' ' : '';
        $class .= 'autocomplete-taille typeahead';
        $attr['class'] = $class;
        $view->vars['attr'] = $attr;
    }

    public function getParent()
    {
        return TextType::class;
    } 
}
