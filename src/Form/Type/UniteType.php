<?php

namespace App\Form\Type;

use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use App\Repository\UniteRepository;
use Symfony\Component\Form\FormBuilderInterface;
use App\Form\DataTransformer\UniteTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;

class UniteType extends AbstractType
{
    /**@var UniteRepository */
    private $uniteRepository;
    public function __construct(UniteRepository $uniteRepository)
    {
        $this->uniteRepository = $uniteRepository;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new CollectionToArrayTransformer(),true)
            ->addModelTransformer(new UniteTransformer($this->uniteRepository),true)
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        $class = isset($attr['class']) ? $attr['class'].' ' : '';
        $class .= 'autocomplete-unite';
        $attr['class'] = $class;

        $view->vars['attr'] = $attr;
    }

    public function getParent()
    {
        return TextType::class;
    }   
}