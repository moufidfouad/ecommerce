<?php

namespace App\Form\Type;

use App\Entity\PromotionItem;
use App\Form\Entity\VariationEntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PromotionItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prix',NumberType::class,[
                'label' => 'entity.promotion_item.form.fields.prix'
            ])
            ->add('produit',VariationEntityType::class,[
                'label' => 'entity.promotion_item.form.fields.produit'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PromotionItem::class,
        ]);
    }
}
