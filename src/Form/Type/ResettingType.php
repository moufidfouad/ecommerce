<?php

namespace App\Form\Type;

use App\Entity\Security\Reset;
use App\Validator\UsernameExists;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ResettingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username',TextType::class,[
                'label' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new UsernameExists([
                        'message' => 'user.username.not_exists'
                    ])
                ],
                'attr' => [
                    'placeholder' => 'security.reset.username'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reset::class,
        ]);
    }
}
