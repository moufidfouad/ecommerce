<?php

namespace App\Repository\Traits;

use LogicException;
use Doctrine\ORM\QueryBuilder;

trait RepositoryTrait
{
    private static function with(QueryBuilder $qb,array $joins)
    {
        if(!empty($joins)){
            foreach($joins as $k => $v){
                $qb
                    ->leftJoin($k,$v)
                    ->addSelect($v)
                ;
            }
        }  
        return $qb;      
    }

    public function getList($qb,array $joins = [])
    {
        if(\is_string($qb)){
            return self::with($this->createQueryBuilder($qb),$joins);
        }elseif($qb instanceof QueryBuilder){
            return self::with($qb,$joins);
        }
        throw new LogicException(sprintf('parameter must be %s',implode(',',['string',QueryBuilder::class])));        
    }
}