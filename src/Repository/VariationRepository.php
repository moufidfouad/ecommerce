<?php

namespace App\Repository;

use App\Entity\Variation;
use App\Entity\Search\VariationSearch;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Traits\RepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Variation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Variation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Variation[]    findAll()
 * @method Variation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariationRepository extends ServiceEntityRepository
{
    use RepositoryTrait;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Variation::class);
    }

    public function findBySearch(VariationSearch $search)
    {
        return $this->getSearchQb($search);
    }

    public function findMinMax(VariationSearch $search)
    {
        $qb = $this->getSearchQb($search,true);
        $qb
            ->select('MIN(variation.prix) as min','MAX(variation.prix) as max')
        ;

        $results = $qb->getQuery()->getScalarResult();

        return [(float) $results[0]['min'],(float) $results[0]['max']];
    }

    public function findOne(string $sku)
    {
        $qb = self::with($this->createQueryBuilder('variation'),[
            'variation.produit' => 'produit',
            'variation.marque' => 'marque',
            'variation.taille' => 'taille',
            'variation.unites' => 'unites',
            'variation.couleurs' => 'couleurs',
        ]);
        $qb
            ->where('variation.sku = :sku')
            ->setParameter('sku',$sku)
        ;
        return $qb;
    }

    private function getSearchQb(VariationSearch $search)
    {
        $qb = self::with($this->createQueryBuilder('variation'),[
            'variation.produit' => 'produit',
            'variation.marque' => 'marque',
            'variation.taille' => 'taille'
        ]);
        $qb
            ->where('variation.quantite > 0')
        ;

        if(!empty($search->getTitre())){
            $qb
                ->andWhere('produit.titre LIKE :titre')
                ->setParameter('titre',"%{$search->getTitre()}%")
            ;               
        }

        if(!empty($search->getMin())){
            $qb
                ->andWhere('variation.prix >= :min')
                ->setParameter('min',$search->getMin())
            ;               
        }

        if(!empty($search->getMax())){
            $qb
                ->andWhere('variation.prix <= :max')
                ->setParameter('max',$search->getMax())
            ;               
        }

        if(!empty($search->getOrder())){
            $order = $search->getOrder();
            switch($order){
                case VariationSearch::ORDER_PRICE_TO_LOWER:
                    $qb->orderBy('variation.prix','DESC');
                    break;
                case VariationSearch::ORDER_PRICE_TO_HIGHER:
                    $qb->orderBy('variation.prix','ASC');
                    break;
            }               
        }

        if(!$search->getTailles()->isEmpty()){                
            $qb
                ->andWhere('taille IN (:taille)')
                ->setParameter('taille',$search->getTailles())
            ;               
        }

        if(!$search->getUnites()->isEmpty()){                
            $qb
                ->leftJoin('variation.unites','unites')
                ->addSelect('unites')
                ->andWhere('unites IN (:unites)')
                ->setParameter('unites',$search->getUnites())
            ;               
        }

        if(!$search->getMarques()->isEmpty()){                
            $qb
                ->andWhere('marque IN (:marques)')
                ->setParameter('marques',$search->getMarques())
            ;               
        }

        if(!$search->getCouleurs()->isEmpty()){                
            $qb
                ->leftJoin('variation.couleurs','couleurs')
                ->addSelect('couleurs')
                ->andWhere('couleurs IN (:couleurs)')
                ->setParameter('couleurs',$search->getCouleurs())
            ;               
        }

        return $qb;
    }
}
