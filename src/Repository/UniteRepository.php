<?php

namespace App\Repository;

use App\Entity\Unite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Traits\RepositoryTrait;

/**
 * @method Unite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Unite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Unite[]    findAll()
 * @method Unite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UniteRepository extends ServiceEntityRepository
{
    use RepositoryTrait;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Unite::class);
    }
    public function findByQuantiteNotEmpty()
    {
        $qb = self::with($this->createQueryBuilder('unite'),[
            'unite.variations' => 'variations'
        ]);
        $qb
            ->where('variations.quantite > 0')
        ;
        return $qb;
    }
}
