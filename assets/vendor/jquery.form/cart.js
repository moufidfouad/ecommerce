$(document).ready(function (){
    const submitCartListner = e => {
        e.preventDefault();

        $jsCartBtn = $('.js-cart-btn');
        $jsCartBtn.forEach(button => {
            button.disabled = true;
        });

        $form = $(e.target);

        $form.ajaxSubmit({
            type: 'post',
            success: function(data) { 
                if(data.message.type === 'success'){
                    toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {
                        timeOut: 5000,
                        closeMethod: 'fadeOut',
                        closeEasing: 'swing'
                    }); 
                } 

                if(data.message.type === 'error'){
                    toastr.error('We do have the Kapua suite available.', 'Turtle Bay Resort', {
                        timeOut: 5000,
                        closeMethod: 'fadeOut',
                        closeEasing: 'swing'
                    }); 
                }
                                         
                var $actionsView = $('.js-cart-actions' + data.id);
                $actionsView.html(data.content.cart.forms);

                var $cartHeader = $('.cart-header');
                $cartHeader.html(data.content.cart.header);
                $jsCartBtn.forEach(button => {
                    button.disabled = false;
                });
            },
            error: function(jqXHR, status, error) {
                console.log(error);
                $jsCartBtn.forEach(button => {
                    button.disabled = false;
                });
            }
        });
    };

    $(document).on('submit', 'form.js-add-item-form', submitCartListner);
    $(document).on('submit', 'form.js-edit-item-form', submitCartListner);
    $(document).on('submit', 'form.js-delete-item-form', submitCartListner);            
});