import ajaxSubmit from './../../vendor/jquery.form/jquery.form.min.js';
import toastr from 'toastr';
$(document).on('submit', 'form.js-cart-form', submit => {
    submit.preventDefault();

    $('.js-cart-btn').each((index,button) => {
        button.disabled = true;
    });   

    $(submit.target).ajaxSubmit({
        type: 'post',
        success: function(data) {                                
            toastr.success(data.message.message);
            if(data.message.type === 'success'){
                toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {
                    timeOut: 5000,
                    closeMethod: 'fadeOut',
                    closeEasing: 'swing'
                }); 
            } 

            if(data.message.type === 'error'){
                toastr.error('We do have the Kapua suite available.', 'Turtle Bay Resort', {
                    timeOut: 5000,
                    closeMethod: 'fadeOut',
                    closeEasing: 'swing'
                }); 
            }
                                     
            $('.js-cart-actions' + data.id).html(data.content.cart.forms);

            $('.cart-header').html(data.content.cart.header);

            $('.js-cart-btn').each((index,button) => {
                button.disabled = false;
            }); 
        },
        error: function(jqXHR, status, error) {
            console.log(error);
            $('.js-cart-btn').each((index,button) => {
                button.disabled = false;
            });
        }
    });
});