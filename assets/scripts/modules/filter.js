import { Flipper, spring } from 'flip-toolkit';
/**
 * @property {HTMLElement} pagination
 * @property {HTMLElement} content
 * @property {HTMLFormElement} form
 * @property {number} page
 */
export default class Filter{
    /**
     * @param {HTMLElement} element 
     */
    constructor(element){
            if(element === null){
                return;
            }
    
        this.pagination = element.querySelectorAll('.js-filter-pagination');
        this.pagesize = element.querySelectorAll('.js-filter-pagesize');
        this.sorting = element.querySelectorAll('.js-filter-sorting');
        this.content = element.querySelector('.js-filter-list');
        this.form = element.querySelector('.js-filter-form');
        this.bindEvents();
    }

    bindEvents(){
        const aClickListener = e => {
            if(e.target.tagName === 'A'){
                e.preventDefault();
                this.loadUrl(e.target.getAttribute('href'));
            }
        };
        const selectLimitListener = e => {
            this.loadForm({limit:e.target.value});
        };
        const selectOrderListener = e => {
            this.loadForm({order:e.target.value});
        };
        this.pagination.forEach(pagination => {
            pagination.addEventListener('click',aClickListener)
        });
        this.sorting.forEach(sorting => {
            sorting.addEventListener('change',selectOrderListener)
        });
        this.pagesize.forEach(pagesize => {
            pagesize.addEventListener('change',selectLimitListener)
        });
        this.form.querySelectorAll('input').forEach(input => {
            input.addEventListener('change',this.loadForm.bind(this));
        });
    }

    getUrlParams(formData,object = {}){
        const params = new URLSearchParams();
        formData.forEach((value,key) => {
            params.append(key,value);
        }); 
        Object.keys(object).forEach((key) => {
            params.append(key,object[key]);
        });  
        return params;    
    }

    async loadForm(object = {}){
        const data = new FormData(this.form);        
        const url = new URL(this.form.getAttribute('action') || window.location.href);
        const params = this.getUrlParams(data,object);
        return this.loadUrl(url.pathname +  '?' + params.toString());
    }

    async loadUrl(url){
        this.showLoader();
        const params = new URLSearchParams(url.split('?')[1] || '');
        params.set('ajax',1);
        const response = await fetch(url.split('?')[0] + '?' + params.toString(),{
            headers: {
                'X-Requested-With':'XMLHttpRequest'
            }
        });

        if(response.status >= 200 && response.status < 300){
            const data = await response.json();
            this.content.innerHTML = data.content;
            this.pagination.forEach(el => {
                el.innerHTML = data.pagination;
            });
            this.pagesize.forEach(el => {
                el.innerHTML = data.pagesize;
            });
            this.sorting.forEach(el => {
                el.innerHTML = data.sorting;
            });  
            this.updatePrices(data);       
            params.delete('ajax');
            history.replaceState({},'',url.split('?')[0] + '?' + params.toString());
        }else{
            console.error(response);
        }
        this.hideLoader(); 
    }

    updatePrices(data){
        const slider = document.getElementById('price-slider');
        if(slider === null){
            return;
        }
        slider.noUiSlider.updateOptions({
            range: {
                min: [data.min],
                max: [data.max]
            }
        });
    }

    flipContent(content){
        const springConfig = 'veryGentle';
        const exitSpring = (element,index,onComplete) => {
            spring({
                config: springConfig,
                values: {
                  translateY: [0, -20],
                  opacity: [1, 0]
                },
                onUpdate: ({ translateY, opacity }) => {
                    element.style.opacity = opacity;
                    element.style.transform = `translateY(${translateY}px)`;
                },
                onComplete
              });
        };
        const appearSpring = (element,index) => {
            spring({
                config: springConfig,
                values: {
                  translateY: [20, 0],
                  opacity: [0, 1]
                },
                onUpdate: ({ translateY, opacity }) => {
                    element.style.opacity = opacity;
                    element.style.transform = `translateY(${translateY}px)`;
                },
                delay: index * 6
              });
        };
        const flipper = new Flipper({
            element: this.content
        });
        Array.from(this.content.children).forEach(element => {
            flipper.addFlipped({
                element,
                spring: springConfig,
                flipId: element.id,
                shouldFlip: false,
                onExit: exitSpring
            });
        });
        flipper.recordBeforeUpdate();
        this.content.innerHTML = content;
        Array.from(this.content.children).forEach(element => {
            flipper.addFlipped({
                element,
                spring: springConfig,
                flipId: element.id,
                onAppear: appearSpring
            });
        });
        flipper.update();
    }

    showLoader(){
        const content = document.querySelector('.js-loader');
        if(content === null){
            return;
        }
        const loader = content.querySelector('#loader');
        if(loader === null){
            return;
        }
        content.classList.add('is-loading');
        loader.style.display = null;
    }

    hideLoader(){
        const content = document.querySelector('.js-loader');
        if(content === null){
            return;
        }
        const loader = content.querySelector('#loader');
        if(loader === null){
            return;
        }
        content.classList.remove('is-loading');
        loader.style.display = 'none';
    }
}