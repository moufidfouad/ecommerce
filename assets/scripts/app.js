const $ = require('jquery');
global.$ = global.jQuery = $;
require('popper.js');

require('bootstrap');
require('@fortawesome/fontawesome-free/js/all.js');
require('jquery-nice-select');
require('jquery-sticky');
require('./../vendor/karma/js/main.js');
import regeneratorRuntime from 'regenerator-runtime';
import Filter from './modules/filter';
require('./modules/cart');
const filter = new Filter(document.querySelector('.js-filter'));
const noUiSlider = require('nouislider');

document.onreadystatechange = () => {
    if(document.readyState !== 'complete'){
        filter.showLoader();
    }else{
        filter.hideLoader();
    }
};

const priceslider = document.getElementById('price-slider');

if(priceslider){
    const min = document.getElementById('min');
    const max = document.getElementById('max');
    const minValue = Math.floor(parseFloat(priceslider.dataset.min));
    const maxValue = Math.ceil(parseFloat(priceslider.dataset.max));

    const range = noUiSlider.create(priceslider, {
        start: [Math.ceil(min.value) || minValue, Math.floor(max.value) || maxValue],
        connect: true,
        range: {
            min: minValue,
            max: maxValue
        }
    });

    range.on('slide',function(values,handle){
        if(handle === 0){
            min.value = Math.round(values[0]);
        }

        if(handle === 1){
            max.value = Math.round(values[1]);
        }
    });

    range.on('end',function(values,handle){
        min.dispatchEvent(new Event('change'));
    });
}
