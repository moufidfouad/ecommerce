/**
 * Load Symfony Encore
 *
 * @type {Encore|Proxy}
 */
let Encore = require('@symfony/webpack-encore');

/**
 * Admin Assets Config
 */
Encore
    // Set Output Path
    .setOutputPath('public/build/')
    .setPublicPath('/build')


    .addStyleEntry('css/karma/app', './assets/styles/app.scss')
    .addEntry('js/karma/app', './assets/scripts/app.js')

    .copyFiles({
        from: './assets/vendor/jquery.form',
        to: 'vendor/ajax-submit/[path][name].[ext]'
    })

    .copyFiles({
        from: './assets/vendor/flashy',
        to: 'vendor/flashy/[path][name].[ext]'
    })

    .copyFiles({
        from: './assets/vendor/typeahead',
        to: 'vendor/typeahead/[path][name].[ext]'
    })

    .copyFiles({
        from: './assets/vendor/tagsinput',
        to: 'vendor/tagsinput/[path][name].[ext]'
    })

    .copyFiles({
        from: './assets/vendor/jquery.collection',
        to: 'vendor/jquery.collection/[path][name].[ext]'
    })

    .copyFiles({
        from: './assets/vendor/select2-bootstrap4-theme',
        to: 'vendor/select2-bootstrap4-theme/[path][name].[ext]'
    })

    .copyFiles({
        from: './assets/vendor/color.picker',
        to: 'vendor/color.picker/[path][name].[ext]'
    })

    // Configs
    .enableSassLoader()
    .enablePostCssLoader()
    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableVersioning(false)
    .disableSingleRuntimeChunk();

module.exports = Encore.getWebpackConfig();
